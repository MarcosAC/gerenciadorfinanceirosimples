﻿using GalaSoft.MvvmLight.Command;
using GalaSoft.MvvmLight.Views;
using GerenciadorFinanceiroSimples.Model;
using GerenciadorFinanceiroSimples.Repositorio;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace GerenciadorFinanceiroSimples.ViewModel
{
    public class PaginaContaDetalheViewModel : BaseViewModel
    {
        private readonly ContaRepositorio _contaRepositorio;
        private readonly INavigationService _navigationService;
        public PaginaContaDetalheViewModel(ContaRepositorio contaRepositorio, INavigationService navigationService)
        {
            _contaRepositorio = contaRepositorio;
            _navigationService = navigationService;

            PageLoad = new RelayCommand(PageLoadExecute);
            AlterarConta = new RelayCommand(AlterarContaExecute);            
            NovoLancamento = new RelayCommand(() => { _navigationService.NavigateTo(Constantes.PAGINA_LANCAMENTO, null); });
            SelecionarItem = new RelayCommand<object>(SelecionarItemExecute);
        }

        private void SelecionarItemExecute(object obj)
        {
            var item = obj as Lancamento;

            if (item == null) return;

            _navigationService.NavigateTo(Constantes.PAGINA_LANCAMENTO, item.LancamentoId);
        }

        private void AlterarContaExecute()
        {
            _navigationService.NavigateTo(Constantes.PAGINA_CONTA, Parametro);
        }

        private async void PageLoadExecute()
        {
            var id = int.Parse(Parametro.ToString());
            Lancamentos = await _contaRepositorio.listarLancamentos(id);
            var receita = Lancamentos
                .Where(c => c.Tipo == "+")
                .Sum(C => C.Valor);

            var despesa = Lancamentos
                .Where(c => c.Tipo == "-")
                .Sum(c => c.Valor);

            TotalConta = receita - despesa;
        }

        public RelayCommand AlterarConta { get; private set; }
        public RelayCommand PageLoad { get; private set; }
        public RelayCommand NovoLancamento { get; private set; }
        public RelayCommand<object> SelecionarItem { get; private set; }       

        private IReadOnlyList<Lancamento> _Lancamentos;

        private double _totalConta;
        public IReadOnlyList<Lancamento> Lancamentos
        {
            get { return _Lancamentos; }
            set { Set(() => Lancamentos, ref _Lancamentos, value); }
        }
        public double TotalConta
        {
            get { return _totalConta; }
            set { Set(ref _totalConta, value); }
        }
    }
}
