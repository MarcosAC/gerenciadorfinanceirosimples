﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Windows.Storage;

namespace GerenciadorFinanceiroSimples.Model
{
    public class Constantes
    {
        public static string DatabasePath = Path.Combine(new[] { ApplicationData.Current.LocalFolder.Path, "database", "gerenciadorfinanceiro.sqlite" });
        internal static string PAGINA_CONTA = "PaginaConta";
        internal static string PAGINA_CONTA_DETALHES = "PaginaContaDetalhes";
        internal static string PAGINA_LANCAMENTO = "PaginaLancamento";        
    }
}
